ChickenDinner
=============

A sample application to learn development on a scala/akka/spray stack using SBT.  

Overview
========

This is a application I am writing to learn some basics on this stack.  The hypothetical scenario (semi-interesting
fake problem) will be motivating employees to be on time with chicken.  This is particularily odd as I am vegan - moving on.

For each day of some arbitrary period of time an employee can earn an entry into a drawing for a chicken dinner each day
they arrive on time.  At the end of the period, a winner will be selected at random from the pool of all entries.

Stack Details as of 18 July 2014
================================

Scala 2.10.3
Akka 2.2.4
Spray 1.2.1
SBT 0.13.5

DRAFT User Stories
==================

Using a REST interface we will be able to:

1) Add a new employee to the drawing (very small company, so employees identified by name only and we'll look to enforce
uniqueness on name)

2) Increment an employees number of entries by one adding the employee if they do not exist.

3) Increment an employees number of entries by some number (for lazy managers who only update things at weeks end).

4) Fetch a list of current entries as JSON.

5) Select a winner returning results as JSON.

Other things that might be interesting
======================================

a) Select n number of winners

b) Post JSON to add/update multiple users.

c) Add a web interface (Play?)

d) Persistent storage (MongoDB, Redis?)

Other Details
=============

The name is from the semi-famous SNL sketch spoofing Charlie Sheen saying, "Winner, winner - chicken dinner!".  I still find that funny and it was in my head when I came up with this concept.  So there you go.
